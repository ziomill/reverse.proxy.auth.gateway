package it.millsoft.rpagateway.test.services;

import it.millsoft.rpagateway.dto.SolutionCredentialsDTO;
import it.millsoft.rpagateway.services.AwsServices;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.CreateSecretRequest;
import software.amazon.awssdk.services.secretsmanager.model.CreateSecretResponse;
import software.amazon.awssdk.services.secretsmanager.model.DeleteSecretRequest;
import software.amazon.awssdk.services.secretsmanager.model.DeleteSecretResponse;

@ActiveProfiles("local")
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AwsServices_Test {

    private static Logger LOGGER = LoggerFactory.getLogger(AwsServices_Test.class);

    @Autowired
    private AwsServices awsServices;

    @Autowired
    private SecretsManagerClient secretsManager;

    @BeforeEach
    public void createSecret() {
        CreateSecretRequest request = CreateSecretRequest.builder()
                .name("esol_ap35663mp00001_local_secret_rso00001slusers")
                .secretString("{\n" +
                        "  \"DMY_ROBOT_USER_CONSUMER_KEY\":\"klRcyho1fVy10v1rYF5KImAx8eka\",\n" +
                        "  \"DMY_ROBOT_USER_CONSUMER_SECRET\":\"gQXKfab9BFSgH90c1xDIbiOjeY0a\",\n" +
                        "  \"SOLUTION_USER_USERNAME\": \"DMY_ROBOT_USER\",\n" +
                        "  \"SOLUTION_USER_PASSWORD\": \"DMY_ROBOT_USER\"\n" +
                        "}")
                .build();

        CreateSecretResponse response = secretsManager.createSecret(request);
        LOGGER.info("Created secret with ARN: " + response.arn());
        Assertions.assertNotNull(response.arn());
    }

    @AfterEach
    public void deleteSecret() {
        DeleteSecretRequest request = DeleteSecretRequest.builder()
                .secretId("esol_ap35663mp00001_local_secret_rso00001slusers")
                .forceDeleteWithoutRecovery(true)
                .build();
        DeleteSecretResponse response = secretsManager.deleteSecret(request);
        LOGGER.info("Deleted secret with ARN: " + response.arn());
        Assertions.assertNotNull(response.deletionDate());
    }

    @Test
    public void readSolutionCredentialsFromSecret_SecretExists_ShouldReturnCredentials() {
        try {
            SolutionCredentialsDTO result = awsServices.readSolutionCredentialsFromSecret();
            Assertions.assertEquals(result.getConsumerKey(),"klRcyho1fVy10v1rYF5KImAx8eka");
            Assertions.assertEquals(result.getConsumerSecret(),"gQXKfab9BFSgH90c1xDIbiOjeY0a");
            Assertions.assertEquals(result.getUsername(),"DMY_ROBOT_USER");
            Assertions.assertEquals(result.getPassword(),"DMY_ROBOT_USER");
        } catch ( Exception ex) {
            Assertions.fail("This test should return a result...",ex);
        }
    }

}
