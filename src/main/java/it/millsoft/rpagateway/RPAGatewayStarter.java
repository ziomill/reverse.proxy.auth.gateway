package it.millsoft.rpagateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RPAGatewayStarter {

    public static void main(String[] args) {
        ConfigurableApplicationContext appContext = SpringApplication.run(RPAGatewayStarter.class, args);
    }

}
