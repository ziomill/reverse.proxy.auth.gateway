package it.millsoft.rpagateway.utils;

public class RPAGatewayException extends Exception {

   private RPAError error;

   public RPAGatewayException(Throwable throwable,
                              RPAError error) {
       super(throwable);
       this.error = error;
   }

    public RPAGatewayException(RPAError error) {
        super();
        this.error = error;
    }

    public RPAError getError() {
        return error;
    }
}
