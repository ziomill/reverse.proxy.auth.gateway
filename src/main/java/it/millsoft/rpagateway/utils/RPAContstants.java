package it.millsoft.rpagateway.utils;

public class RPAContstants {

    public static class PARAMS {
        public static final String GRANT_TYPE_KEY = "grant_type";
        public static final String USERNAME_KEY = "username";
        public static final String PASSWORD_KEY = "password";
        public static final String SCOPE_KEY = "scope";
    }

    public static class HEADERS {

        public static class SECUREPLAT {
            public static final String LOGGED_USER = "LOGGED_USER";
            public static final String SOLUTION = "SOLUTION";
            public static final String TERRITORY = "TERRITORY";
        }

    }

}
