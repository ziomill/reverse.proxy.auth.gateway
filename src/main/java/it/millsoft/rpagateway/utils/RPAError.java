package it.millsoft.rpagateway.utils;

public enum RPAError {

    READ_SECRET_ERROR("RE01"),
    TOKEN_GENERATION_ERROR("RE02");

    private String value;

    RPAError(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
