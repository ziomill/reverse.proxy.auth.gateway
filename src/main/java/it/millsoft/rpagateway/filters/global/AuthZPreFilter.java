package it.millsoft.rpagateway.filters.global;

import it.millsoft.rpagateway.RPAGatewayStarter;
import it.millsoft.rpagateway.dto.SolutionCredentialsDTO;
import it.millsoft.rpagateway.services.ApiManagerServices;
import it.millsoft.rpagateway.services.AwsServices;
import it.millsoft.rpagateway.utils.RPAContstants;
import it.millsoft.rpagateway.utils.RPAGatewayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthZPreFilter implements GlobalFilter, Ordered {

    final static Logger LOGGER = LoggerFactory.getLogger(AuthZPreFilter.class);

    @Autowired
    private ApiManagerServices apiManagerServices;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        LOGGER.info("Start: Global AuthZ Pre-Filter");

        // 1. Get USER and SOLUTION from headers
        String loggedUser = exchange.getRequest().getHeaders().getFirst(RPAContstants.HEADERS.SECUREPLAT.LOGGED_USER);
        String solution = exchange.getRequest().getHeaders().getFirst(RPAContstants.HEADERS.SECUREPLAT.SOLUTION);
        String territory = exchange.getRequest().getHeaders().getFirst(RPAContstants.HEADERS.SECUREPLAT.TERRITORY);
        LOGGER.info("Start Authorization filtering for request having: " + "\n" +
                    "\tResource  : " + exchange.getRequest().getURI()    + "\n" +
                    "\tUser      : " + loggedUser                        + "\n" +
                    "\tSolution  : " + solution                          + "\n" +
                    "\tTerritory : " + territory);

        // 2. Get USER's permissions (activities) on SecurePlat {LOGGED_USER -- SOLUTION - TERRITORY}
        Set<String> userPermissions = getMockPermissions();
        LOGGER.info("User: " + loggedUser + " " +
                        " has following permissions: " + userPermissions.stream().collect(Collectors.joining(",", "{", "}")) +
                            " on Solution: " + solution +
                                " for Territory: " + territory);

        // 3. Robot User Authentication on WSO2 (using scopes)
        String token = null;
        try {
            token = apiManagerServices.getToken(userPermissions);
            LOGGER.info("User: " + loggedUser + " is authenticated. Generated token is: " + token);
        }
        catch (RPAGatewayException ex) {
            LOGGER.error("There was an error generating the token: " + ex.getMessage());
            exchange.getResponse().getHeaders().set("status", "401");
            return Mono.empty();
        }

        // 4. Enrich Original Request Header with Auth Bearer
        ServerHttpRequest enrichedRequest = exchange.getRequest()
                                                    .mutate()
                                                    .header("Authorization", "Bearer " + token)
                                                    .build();

        // 5. Route Enriched Request to WSO2 Api Gateway
        return chain.filter(exchange.mutate().request(enrichedRequest).build());
    }

    @Override
    public int getOrder() {
        return 0;
    }

    //TODO: Mocked call to SecurePlat API
    private Set<String> getMockPermissions() {
        return Set.of("p1","p2","p3","p4");
    }
}

