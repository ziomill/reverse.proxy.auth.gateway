package it.millsoft.rpagateway.dto;

public class SolutionCredentialsDTO {

    private String username;
    private String password;
    private String consumerKey;
    private String consumerSecret;

    public SolutionCredentialsDTO(String username, String password, String consumerKey, String consumerSecret) {
        this.username = username;
        this.password = password;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

}
