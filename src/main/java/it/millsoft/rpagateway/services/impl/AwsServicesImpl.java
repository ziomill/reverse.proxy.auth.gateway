package it.millsoft.rpagateway.services.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.millsoft.rpagateway.dto.SolutionCredentialsDTO;
import it.millsoft.rpagateway.services.AwsServices;
import it.millsoft.rpagateway.utils.RPAError;
import it.millsoft.rpagateway.utils.RPAGatewayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.*;

import java.io.IOException;

@Service
public class AwsServicesImpl implements AwsServices {

    private static Logger LOGGER = LoggerFactory.getLogger(AwsServicesImpl.class);

    @Value("${spring.aws.secretsmanager.secretName}")
    private String awsSmSecretName;

    @Autowired
    private SecretsManagerClient secretsManager;

    @Override
    public SolutionCredentialsDTO readSolutionCredentialsFromSecret() throws RPAGatewayException {
        try  {
            // AWS Request creation
            GetSecretValueRequest getSecretValueRequest  =  GetSecretValueRequest.builder().secretId(awsSmSecretName).build();

            // Call to SecretManager service
            GetSecretValueResponse getSecretValueResponse  =  secretsManager.getSecretValue(getSecretValueRequest);

            if(getSecretValueResponse == null) {
                LOGGER.error("The Secret Response returned is null");
                throw new RPAGatewayException(RPAError.READ_SECRET_ERROR);
            }

            // Read Secret Response
            // Decrypted secret using the associated KMS CMK // Depending on whether the secret was a string or binary, one of these fields will be populated
            String secret = getSecretValueResponse.secretString();
            if(secret == null) {
                LOGGER.error("The Secret String returned is null");
                throw new RPAGatewayException(RPAError.READ_SECRET_ERROR);
            }

            ObjectMapper objectMapper  =  new  ObjectMapper();
            JsonNode secretsJson  =  objectMapper.readTree(secret);

            String username =  secretsJson.get("SOLUTION_USER_USERNAME").textValue();
            String password =  secretsJson.get("SOLUTION_USER_PASSWORD").textValue();
            String consumerKey =  secretsJson.get(username + "_CONSUMER_KEY").textValue();
            String consumerSecret = secretsJson.get(username + "_CONSUMER_SECRET").textValue();

            return new SolutionCredentialsDTO(username,password,consumerKey,consumerSecret);
        }
        catch  (ResourceNotFoundException ex)  {
            LOGGER.error("The requested secret "  +  awsSmSecretName  +  " was not found");
            throw new RPAGatewayException(ex, RPAError.READ_SECRET_ERROR);
        }
        catch  (InvalidRequestException ex)  {
            LOGGER.error("The request was invalid due to: "  +  ex.getMessage());
            throw new RPAGatewayException(ex, RPAError.READ_SECRET_ERROR);
        }
        catch  (InvalidParameterException ex)  {
            LOGGER.error("The request had invalid params: "  +  ex.getMessage());
            throw new RPAGatewayException(ex, RPAError.READ_SECRET_ERROR);
        }
        catch  (IOException ex)  {
            LOGGER.error("Exception while retrieving secret values: "  +  ex.getMessage());
            throw new RPAGatewayException(ex, RPAError.READ_SECRET_ERROR);
        }
    }

}
