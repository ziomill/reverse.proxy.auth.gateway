package it.millsoft.rpagateway.services.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.millsoft.rpagateway.dto.SolutionCredentialsDTO;
import it.millsoft.rpagateway.services.ApiManagerServices;
import it.millsoft.rpagateway.services.AwsServices;
import it.millsoft.rpagateway.utils.RPAContstants;
import it.millsoft.rpagateway.utils.RPAError;
import it.millsoft.rpagateway.utils.RPAGatewayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ApiManagerServicesImpl implements ApiManagerServices {

    final static Logger LOGGER = LoggerFactory.getLogger(ApiManagerServices.class);

    @Value("${rpagateway.api_manager_base_url}")
    private String apiManagerBaseURL;

    @Value("${rpagateway.api_manager_get_token_resource}")
    private String apiManagerGetTokenResource;

    @Autowired
    private AwsServices awsServices;

    @Override
    public String getToken(Set<String> scopes) throws RPAGatewayException {
        // Get Solution's Robot user credential from Secret
        SolutionCredentialsDTO credentials = awsServices.readSolutionCredentialsFromSecret();

        // Http client prepared with request
        HttpClient client = httpClient(credentials.getConsumerKey(),credentials.getConsumerSecret());
        String form = form(credentials.getUsername(),credentials.getPassword(),scopes);
        HttpRequest request = request(form);

        // Api Manager call to get token
        try {
            LOGGER.info("Ready to get Token...");
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if(HttpStatus.resolve(response.statusCode()).equals(HttpStatus.OK)) {
                LOGGER.info("Authentication done with success: " + response.body());
                ObjectMapper objectMapper  =  new  ObjectMapper();
                JsonNode secretsJson  =  objectMapper.readTree(response.body());
                String token =  secretsJson.get("access_token").textValue();
                return token;
            } else {
                LOGGER.error("Authentication error: " + response.statusCode() + " - " + response);
                throw new RPAGatewayException(RPAError.TOKEN_GENERATION_ERROR);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw new RPAGatewayException(ex, RPAError.TOKEN_GENERATION_ERROR);
        }
    }


    private HttpClient httpClient(String consumerKey, String consumerSecret) {
        HttpClient result = HttpClient.newBuilder()
                                      .version(HttpClient.Version.HTTP_1_1)
                                      .authenticator(new Authenticator() {
                                            @Override
                                            protected PasswordAuthentication getPasswordAuthentication() {
                                                return new PasswordAuthentication(consumerKey,
                                                                                  consumerSecret.toCharArray());
                                            }
                                      }).build();
        LOGGER.debug("Http client created ...");
        return result;
    }

    private String form(String username, String password, Set<String> scopes) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(RPAContstants.PARAMS.GRANT_TYPE_KEY, "password");
        parameters.put(RPAContstants.PARAMS.USERNAME_KEY, username);
        parameters.put(RPAContstants.PARAMS.PASSWORD_KEY, password);
        if(scopes != null && scopes.size() > 0) {
            String scopesAsString = scopes.stream().collect(Collectors.joining(" "));
            parameters.put(RPAContstants.PARAMS.SCOPE_KEY, scopesAsString);
        }

        String result = parameters.keySet().stream()
                                            .map(key -> URLEncoder.encode(key, StandardCharsets.UTF_8) + "=" + URLEncoder.encode(parameters.get(key), StandardCharsets.UTF_8))
                                            .collect(Collectors.joining("&"));
        LOGGER.debug("Form created ...");
        return result;
    }

    private HttpRequest request(String form) {
        String getTokenUrl = apiManagerBaseURL + apiManagerGetTokenResource;
        LOGGER.debug("Api Manager GetToken URL: " + getTokenUrl);
        HttpRequest result = HttpRequest.newBuilder()
                                        .uri(URI.create(getTokenUrl))
                                        .timeout(Duration.ofMinutes(1))
                                        .header("Content-Type", "application/x-www-form-urlencoded")
                                        .POST(HttpRequest.BodyPublishers.ofString(form))
                                        .build();
        LOGGER.debug("Request created ...");
        return result;
    }

}
