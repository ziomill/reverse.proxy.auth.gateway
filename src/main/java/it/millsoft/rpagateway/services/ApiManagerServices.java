package it.millsoft.rpagateway.services;

import it.millsoft.rpagateway.utils.RPAGatewayException;

import java.util.Set;

public interface ApiManagerServices {

    String getToken(Set<String> scopes) throws RPAGatewayException;

}
