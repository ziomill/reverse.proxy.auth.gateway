package it.millsoft.rpagateway.services;

import it.millsoft.rpagateway.dto.SolutionCredentialsDTO;
import it.millsoft.rpagateway.utils.RPAGatewayException;

public interface AwsServices {

    SolutionCredentialsDTO readSolutionCredentialsFromSecret() throws RPAGatewayException;

}
