package it.millsoft.rpagateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;

import java.net.URI;

@Configuration
public class RPAGatewayConfig {

    @Configuration
    public static class AwsConfig {

        @Value("${spring.aws.secretsmanager.endpoint}")
        private String awsSmEndpoint;
        @Value("${spring.aws.secretsmanager.region}")
        private String awsSmRegion;

//        @Bean
//        @Primary
//        public AWSCredentialsProvider awsCredentialsProvider() {
//            return new STSAssumeRoleSessionCredentialsProvider.Builder(roleArn, roleSession).build();
//        }

        @Bean
        @Primary
        public SecretsManagerClient awsSecretsManager() {
            return SecretsManagerClient.builder()
                                       .region(Region.of(awsSmRegion))
                                       .endpointOverride(URI.create(awsSmEndpoint))
                                       .build();
        }

    }

}
